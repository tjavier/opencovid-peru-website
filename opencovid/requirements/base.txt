Django==3.1
django-environ==0.4.5
gunicorn==20.0.4
psycopg2-binary==2.8.5
django-ordered-model==3.4.1
django-storages[google]==1.9.1
google-cloud-secret-manager==1.0.0
django-tinymce==3.0.2
django-image-cropping==1.5.0
easy-thumbnails==2.7
django-solo==1.1.3
django-filer==2.0.2

# old dependency to make migration work
django-ckeditor==5.9.0
