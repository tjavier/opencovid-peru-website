from reportes.models import ReportPage
from .constants import REGIONES

def reports(request):
    return { 'reports' : ReportPage.objects.filter(published_at__isnull=False) }

def regions(request):
    return { 'regions': REGIONES }
