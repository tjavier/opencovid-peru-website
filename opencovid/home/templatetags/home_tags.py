from django import template

register = template.Library()

@register.filter(name='replace_with_char_padding')
def replace_with_char_padding(integer_value):
    """Replaces integer value with provided char padding"""
    return ''.ljust(len(str(integer_value)), '0')