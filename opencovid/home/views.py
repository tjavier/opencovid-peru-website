from django.shortcuts import render

from .models import DashboardSummary


def home_view(request):
    summary = DashboardSummary.get_solo()
    
    return render(request, 'index.html', {        
        'SUMMARY' : summary.toDict(),
    })

def team_view(request):
    return render(request, 'team.html')
