from django.contrib import admin

from solo.admin import SingletonModelAdmin

from .models import DashboardSummary

admin.site.register(DashboardSummary, SingletonModelAdmin)
