from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.utils.text import slugify

from tinymce.models import HTMLField
from ordered_model.models import OrderedModel
from image_cropping import ImageRatioField

class ReportPage(OrderedModel):
    """
    Contenido de Página de Reporte
    """

    class ReportHTMLTemplate(models.TextChoices):
        """
        Valid options for pre-made templates
        """
        GENERIC = 'reporte_generic.html', _('Generic')
        MAPA_SEMAFORO = 'reporte_semaforo_mapa.html', _('Semaphore Map')
        DESFASE = 'reporte_regions_desfase.html', _('Desfase')
        POSITIVIDAD = 'reporte_regions_positividad.html', _('Positividad')
        CAMAS_UCI = 'reporte_regions_camas_uci.html', _('Camas UCI')
        SINADEF = 'reporte_regions_SINADEF.html', _('SINADEF')
        OXIGENO = 'reporte_regions_oxigeno.html', _('Oxígeno')
        MOBILITY = 'reporte_regions_mobility.html', _('Mobility')

    created_at = models.DateTimeField(auto_now_add=True,
        help_text='Time (UTC) report was created')
    updated_at = models.DateTimeField(auto_now=True,
        help_text='Time (UTC) report was last updated')
    published_at = models.DateTimeField(blank=True, 
        null=True, 
        help_text='Date/Time (UTC) report was made visible/published. Empty if not published')
    title = models.CharField(max_length=30, unique=True, help_text='Enter Title of Report')
    description = models.CharField(max_length=128, help_text='Enter (short) description of content')
    slug = models.SlugField(max_length = 200, 
        unique=True, blank=False, 
        help_text='Enter a URL for report. Ex entering "sinadef" will generate http://opencovid-peru.com/reportes/sinadef')
    cover_image = models.ImageField(blank=True, upload_to='report_files/cover_images', 
        help_text='Image to use a cover in homepage')
    cropped_cover_image = ImageRatioField('cover_image', '508x225', size_warning=True,
        help_text='Height will be constant in all devices. Max width is [508x225] (*smaller* devices). [288x225] is normal')
    merge_Lima_Region_and_Lima_Metropolitana = models.BooleanField(blank=False, null=False, default=False,
        help_text="If we have to only show Lima as an option (not Lima Metropolitana) and take the values of Lima Region")
    html_report_template = models.CharField(
        max_length=120,
        choices=ReportHTMLTemplate.choices,
        default=ReportHTMLTemplate.GENERIC,
        blank=False,null=False,
        help_text='Template used to render report')
    table_of_contents = models.TextField(blank=True, null=True,
        help_text='Optional field to put raw HTML to be used as Table of Contents')
    top_body_content = HTMLField(blank=True, null=True,
        help_text='Text at the top of the page (before our charts)')
    main_body_content = models.TextField(blank=True, null=True,
        help_text='Raw HTML content to be inserted in the middle of page')
    bottom_body_content = HTMLField(blank=True, null=True,
        help_text='Text at the bottom of the page (after our charts)')


    class Meta(OrderedModel.Meta):
        pass

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = self.slug or slugify(self.title)
        super().save(*args, **kwargs)

    @property
    def published(self):
        if self.published_at is None:
            return False
        return True
    
    @published.setter
    def published(self, value):
        if self.published_at is None and value is True:
            self.published_at = timezone.now()
        else:
            self.published_at = None

            


    