from django.urls import path

from .views import nacional_view

urlpatterns = [    
    # this is meant to be a catch all to dynamically pull report
    path('', nacional_view, name='nacional'),
]
