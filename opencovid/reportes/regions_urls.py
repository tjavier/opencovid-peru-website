from django.urls import path

from .views import find_region_dynamically_view

urlpatterns = [    
    # this is meant to be a catch all to dynamically pull report
    path('<str:region_name>/', find_region_dynamically_view, name='find_region'),
]