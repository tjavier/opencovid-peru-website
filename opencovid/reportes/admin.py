from django.contrib import admin

from ordered_model.admin import OrderedModelAdmin
from image_cropping import ImageCroppingMixin

from .models import ReportPage


def publish_report(modeladmin, request, queryset):
    for report in queryset.all():
        report.published = True
        report.save()
        
publish_report.short_description = "Publish selected reports"

def unpublish_report(modeladmin, request, queryset):
    for report in queryset.all():
        report.published = False
        report.save()

unpublish_report.short_description = "Unpublish selected reports"

class ReportPageAdmin(ImageCroppingMixin, OrderedModelAdmin):
    prepopulated_fields = {'slug': ('title',)}
    list_display = ('title', 'slug', 'html_report_template', 'published', 'move_up_down_links')
    readonly_fields = ('published_at',)

    actions = [publish_report, unpublish_report]

    # shadowing function to get pretty boolean display
    def published(self, instance):
        return instance.published
    published.boolean = True


admin.site.register(ReportPage, ReportPageAdmin)
